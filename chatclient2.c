#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>

#include <Xm/Text.h>
#include <Xm/TextF.h>
#include <Xm/LabelG.h>
#include <Xm/RowColumn.h>
#include <X11/Xos.h> 

//GLOBALS 
#define kMAX_SOCK  32
#define kMAX_BUFF  300

//status macros
const char kINIT[] = "$INIT";
const char kOK[] = "$OK";
const char kERR[] = "$ERR";
const char kNOT_STARTED[] = "$NOT_STARTED";
const char kRUNNING[] = "$RUNNING";
const char kSTOPPED[] = "$STOPPED";

//others
int gSocket; //socket
int gStatus = kNOT_STARTED; //status of gui
char *username; 
char fullMsg[500];
struct sockaddr_in server;
pthread_cond_t gStart;
pthread_mutex_t gLock;



//widget globals

Widget toplevel, rowcol_v, rowcol_h, label_w; //gui components
Widget text_w, msg_w, text_output; //text area and message area
XtAppContext   app;
int            i, n;
Arg            args[10];

//callback function for the text box widget
void append_msg(Widget widget, XtPointer client_data, XtPointer call_data){

	char* message;
	message = XmTextGetString(msg_w);
	printf("%s\n", message);
	XmTextPosition position = XmTextGetLastPosition(text_w);
	int len = strlen(message);

	if (len == 0)
	{XmTextSetString(msg_w, "Please enter a message"); return;}
	if (len > 300) 
	{XmTextSetString(msg_w, "Message too long!"); return;}

	//appends username to the message
	strcpy(fullMsg, username);
	strcat(fullMsg, ": ");
	strcat(fullMsg, message);
	strcat(fullMsg, "\n");

	if(write(gSocket, fullMsg, kMAX_BUFF) < 0) { printf("Could not write\n"); }
	memset(message, 0, sizeof(message));
	memset(fullMsg, 0, sizeof(fullMsg));
	XmTextSetString(msg_w, "");
	//memset(message, 0, sizeof(message));

	//alerts the GUI thread to start running
	pthread_mutex_lock(&gLock);
	gStatus = kRUNNING;
	pthread_cond_signal(&gStart);
	pthread_mutex_unlock(&gLock);

	

	
}




//preliminary actions to connect to the chat server
int connectToChatServer()
{
	
	gSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (gSocket < 0) {printf("Could not create socket\n"); return 1;}

	server.sin_addr.s_addr = inet_addr("127.0.0.1");	
	bzero((char*)&server, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_port = htons ( 8801 ); 

	if (connect(gSocket, (struct sockaddr *)&server, sizeof(server)) < 0)
	{printf("Failed to connect\n"); return 1;}

	return 0;
}


void drawGUI()
{

	//initialize
	int argc = 0; char **argv= "hello";

	//make the window
	 rowcol_v = XmCreateRowColumn (toplevel, "rowcol_v", NULL, 0);
   	 XtSetArg (args[0], XmNorientation, XmHORIZONTAL); 
   	 rowcol_h = XmCreateRowColumn (rowcol_v, "rowcol_h", args, 1);
   	 label_w = XmCreateLabelGadget (rowcol_h, "Enter Message:", NULL, 0);
   	 XtManageChild (label_w);
   	 msg_w = XmCreateTextField (rowcol_h, "send_msg", NULL, 0);
   	 XtManageChild (msg_w);
   	 XtManageChild (rowcol_h);

	//sets formatting parameters
   	 n = 0;
   	 XtSetArg (args[n], XmNeditable, False);              n++;
   	 XtSetArg (args[n], XmNcursorPositionVisible, False); n++;
   	 XtSetArg (args[n], XmNshadowThickness, 0);           n++;
   	 XtSetArg (args[n], XmNhighlightThickness, 0);        n++;
   	 text_output = XmCreateText (rowcol_v, "text_output", args, n);
   	 XtManageChild (text_output);

   	 n = 0;
   	 XtSetArg (args[n], XmNrows, 10);                    n++;
   	 XtSetArg (args[n], XmNcolumns, 80);                 n++;
   	 XtSetArg (args[n], XmNeditMode, XmMULTI_LINE_EDIT); n++;
   	 XtSetArg (args[n], XmNscrollHorizontal, False);     n++;
   	 XtSetArg (args[n], XmNwordWrap, True);              n++;
   	 text_w = XmCreateScrolledText (rowcol_v, "text_w", args, n);
   	 XtManageChild (text_w);

	//associates the callback function with the Return key
   	 XtAddCallback (msg_w, XmNactivateCallback, append_msg, (XtPointer) msg_w);
	
         XtManageChild (rowcol_v);
	 XtRealizeWidget (toplevel);
}




void *run_gui(void *arg) {

	drawGUI();
	XmTextSetString(msg_w, "Press return to connect."); 
	XtAppMainLoop (app);

	return NULL;
}

//listener thread 
void *run_listener(void *arg){
	char message[kMAX_BUFF];
	
	//wait for the GUI to start running	
	pthread_mutex_lock(&gLock);
	while (gStatus == kNOT_STARTED)
		pthread_cond_wait(&gStart, &gLock);

	pthread_mutex_unlock(&gLock);

	while(1) {
		
		read(gSocket, message, kMAX_BUFF);
		//inserts message
		XmTextPosition position = XmTextGetLastPosition(text_w) + 1;
		XmTextInsert(text_w, position, message);

		if(gStatus == kSTOPPED){ close(gSocket); break; }
	}

	return NULL;
}


int main(int argc, char *argv[])
{	
	//prompts for username
	if (argc != 2 ) {printf("Please enter a username.\n"); return 1;}
	username = argv[1];
	//creates threads and attributes
	if (connectToChatServer() == 0) {
		pthread_attr_t tattr;
		pthread_attr_init(&tattr);
		pthread_attr_setscope(&tattr, PTHREAD_SCOPE_SYSTEM);
		pthread_cond_init(&gStart, NULL);
		pthread_mutex_init(&gLock, NULL);
		pthread_t tgui, tlistener;
		
		toplevel = XtVaOpenApplication(&app, "Chat", NULL, 0, &argc, argv, NULL,   sessionShellWidgetClass, NULL);
		pthread_create(&tgui, &tattr, run_gui, NULL);
		pthread_create(&tlistener, &tattr, run_listener, NULL);
	
		pthread_join(tgui, NULL);
		pthread_join(tlistener, NULL);	
	}

	return 0;
}
