#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <netdb.h>
#include <netinet/in.h>

#define kMAX_SOCK  32
#define kMAX_BUFF  300

//globals
int intNoSockets = 0;
int intMaxNoSockets = 32;
int intSockets[32];
char strBuff[300];

int tosend; //condition
//lock and condition variables
pthread_mutex_t mlock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t msend = PTHREAD_COND_INITIALIZER;


const char kINIT[] = "$INIT";
const char kOK[] = "$OK";
const char kERR[] = "$ERR";


//adds a connection to the array of connections
int addConnection(int sockfd)
{
	//returns 0 if the number of clients are over 32
	if (intNoSockets == intMaxNoSockets)
		return 0;
	
	intSockets[intNoSockets] = sockfd;
	intNoSockets++;
	return 1;

}

//removes a connection from the array
void DelConnection(int sockfd)
{
	intNoSockets--;
	intSockets[intNoSockets] = 0;
	int i = 0, j = 0;

	while(i < intNoSockets)	
	{
		if(intSockets[i] == sockfd)
		{
			while(j < intNoSockets) {
				intSockets[j] = intSockets[j+1];
				--intNoSockets;
				j++;
			}
			break;
		}
		i++;
	}
	
}

//broadcasts message to all clients
void SendAll()
{
	int i = 0;
	while (i < intNoSockets)
	{
		write(intSockets[i], strBuff, kMAX_BUFF);
		printf("%s\n", strBuff);
		i++;
	}
}

//updates the buffer variable with the new message
void UpdateBuff(char *msg)
{
	if(sizeof(msg) <= kMAX_BUFF){
	bzero(strBuff, kMAX_BUFF);
	strcpy(strBuff, msg);}
}

	

//sender thread
void *run_sender(void *arg){
	while(1){
		pthread_mutex_lock(&mlock);
		while(!tosend){
			pthread_cond_wait(&msend, &mlock);}

	//update connected clients
	SendAll();
	//printf("Sent the thing to everyone :)\n");
	tosend = 0; //tells the sender to stop
	pthread_mutex_unlock(&mlock);		
	}

	return NULL;
}

//client handler
void *handle_child(void *arg){
	//socket to the client
	int cli_sock = (int)arg;
	char buff[300];
	char *msg;

	
	pthread_detach(pthread_self());
	//bzero(buff, sizeof(buff));

	//reads the request from the client
	if(read(cli_sock, buff, 300) > 0) {
		pthread_mutex_lock(&mlock);
		int added = addConnection(cli_sock); //attempts to add the client, checks for capacity
		pthread_mutex_unlock(&mlock);

	if(added == 1) {
	//writes acknowledgment to the client 
		write(cli_sock, kOK, 300);
		while (1)
		{
			if(read(cli_sock, buff, kMAX_BUFF)<= 0)			
				break;
			printf("Message received");
			pthread_mutex_lock(&mlock);
			UpdateBuff(buff); //updates the buffer
			tosend = 1; //activates the sender
			pthread_cond_signal(&msend);
			pthread_mutex_unlock(&mlock);
		}
		
	}
	else {write(cli_sock, kERR, kMAX_BUFF);} 
	}
	//}
	DelConnection(cli_sock);
	
	close(cli_sock);
	return 0;
}


int main(int argc, char *argv[]){

	int listen_sock = 0, cli_sock = 0, i = 0;
	struct sockaddr_in server, client;
	

	//create socket 
	listen_sock = socket(AF_INET, SOCK_STREAM, 0);
	if (listen_sock < 0)
	{printf("Could not create socket\n"); return 1;}

	//Prepare the sockaddr_in struct
	bzero((char *)&server, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = inet_addr("127.0.0.1");
	server.sin_port = htons( 8801 );

	//binding
	if (bind(listen_sock, (struct sockaddr *)&server, sizeof(server)) < 0)
	{printf("Binding failed\n"); return 1; }

	//listening
	listen(listen_sock, kMAX_SOCK);

	printf("Waiting for connections...\n");
	int clilen = sizeof(client);

	
	//creates default attributes to pass to create function
	pthread_attr_t tattr;
	pthread_attr_init(&tattr);
	pthread_attr_setscope(&tattr, PTHREAD_SCOPE_SYSTEM);
	pthread_t sthread;
	pthread_create(&sthread, &tattr, run_sender, NULL);


	while(1)
	{
		cli_sock = accept(listen_sock, (struct sockaddr*)&client, (socklen_t*)&clilen);
		if (cli_sock >0) {
			pthread_t cthread;
			pthread_create(&cthread, &tattr, handle_child, (void*)cli_sock);	
		}
	}



	return 0;
	

}



